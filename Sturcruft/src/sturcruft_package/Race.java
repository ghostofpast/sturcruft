package sturcruft_package;

public enum Race {
	TERRAN,ZERG,PROTOSS;
	
	static Race getRandomRace()
	{
		return Race.intToRace((int)(Math.random()*3.0+1.0));
	}
	
	static Race intToRace(int whatRace)
	{
		if (whatRace == 1)
		{
			return Race.TERRAN;
		}
		if (whatRace == 2)
		{
			return Race.ZERG;
		}
		if (whatRace == 3)
		{
			return Race.PROTOSS;
		}
		return null;
	}
	
	static int raceToInt(Race whatRace)
	{
		if (whatRace == Race.TERRAN)
		{
			return 1;
		}
		if (whatRace == Race.ZERG)
		{
			return 2;
		}
		if (whatRace == Race.PROTOSS)
		{
			return 3;
		}
		return 0;
	}
	
	public String toString()
	{
		if (this == Race.TERRAN)
		{
			return "Terran";
		}
		if (this == Race.ZERG)
		{
			return "Zerg";
		}
		if (this == Race.PROTOSS)
		{
			return "Protoss";
		}
		return "Unkown";
	}
}
