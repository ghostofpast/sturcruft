package sturcruft_package;
import java.util.ArrayList;

public class Base {
	private Player player;
	private String name;
	private int troops, population, lives, grow;
	private ArrayList<Base> access;
	private boolean isAlive;
	private int priority;
	private int strength;
	private boolean attacked;
	private boolean wasAttacked;
	
	Player getPlayer()
	{
		return this.player;
	}
	
	void setPlayer(Player who)
	{
		this.player=who;
	}
	
	void setAttackedStatus(boolean newState)
	{
		this.attacked=newState;
	}
	
	void setWasAttackedStatus(boolean newState)
	{
		this.wasAttacked=newState;
	}
	
	boolean isAttacked()
	{
		return this.attacked;
	}
	
	boolean hasBeenAttacked()
	{
		return this.wasAttacked;
	}
	
	int getStrength()
	{
		return this.strength;
	}
	
	void setStrength(int newStrength)
	{
		this.strength=newStrength;
	}
	
	int getPriority()
	{
		return this.priority;
	}
	
	void setPriority(int newPriority)
	{
		this.priority=newPriority;
	}
	
	boolean isBaseAlive()
	{
		return this.isAlive;
	}
	
	void destroyBase()
	{
		this.isAlive=false;
	}
	String getName()
	{
		return this.name;
	}
	
	void setName(String newName)
	{
		this.name=newName;
	}
	
	int getTroops()
	{
		return this.troops;
	}
	
	void setTroops(int newAmount)
	{
		if (newAmount<0)
		{
			this.troops=0;
			return;
		}
		this.troops=newAmount;
	}
	
	void AddTroops(int howMany)
	{
		this.setTroops(this.getTroops()+howMany);
	}
	
	void removeTroops(int howMany)
	{
		this.AddTroops(-howMany);
	}
	
	void clearTroops()
	{
		this.setTroops(0);
	}
	
	boolean isBaseDefended()
	{
		return this.getTroops()>0;
	}
	
	int getPop()
	{
		return this.population;
	}
	
	void setPop(int newAmount)
	{
		this.population=newAmount;
	}
	
	void addPop(int howMany)
	{
		this.setPop(this.getPop()+howMany);
	}
	
	int getGrow()
	{
		return this.grow;
	}
	
	void setGrow(int howMuch)
	{
		this.grow=howMuch;
	}
	
	int getCurrentgrow()
	{
		return (this.getPop()*this.getGrow())/100;
	}
	
	int getLives()
	{
		return this.lives;
	}
	
	void setLives(int newLives)
	{
		this.lives = newLives;
	}
	
	void hurt()
	{
		this.setLives(this.getLives()-1);
	}
	
	void regenerate()
	{
		this.setLives(3);
	}
	
	Base(Race newRace, String newName)
	{
		this.setName(newName);
		if (newRace == Race.TERRAN)
		{
			this.makeBase(150,10,0,2);
			return;
		}
		if (newRace == Race.ZERG)
		{
			this.makeBase(200,15,2,1);
			return;
		}
		if (newRace == Race.PROTOSS)
		{
			this.makeBase(100,10,1,4);
			return;
		}
	}
	void makeBase(int newPopulation, int growRate, int newPriority, int newStrength) {
		this.setPop(newPopulation);
		this.setTroops(0);
		this.setGrow(growRate);
		this.setPriority(newPriority);
		this.setStrength(newStrength);
		this.regenerate();
		this.isAlive=true;
		this.access=new ArrayList<Base>();
	}
	
	ArrayList<Base> getAccesses()
	{
		return this.access;
	}
	
	void addBase(Base whatBase)
	{
		if (!this.getAccesses().contains(whatBase))
		{
			assert this.getAccesses()!=null;
			this.getAccesses().add(whatBase);
			System.out.println("Base has been added :  "+this.getAccesses().get(0).getName());
			assert(!this.getAccesses().isEmpty());
		}
		if (!whatBase.getAccesses().contains(this))
		{
			assert whatBase.getAccesses()!=null;
			whatBase.getAccesses().add(this);
			System.out.println("Return Base has been added : "+whatBase.getAccesses().get(0).getName());
		}
	}
	
	void linkToBase(Base... whatBase)
	{
		for (int i=0; i<whatBase.length;++i)
		{
			this.addBase(whatBase[i]);
		}
	}
	
	int getPower()
	{
		return this.getTroops()*this.getStrength();
	}
	int processCasualities(Base whatBase)
	{
		if (whatBase.isBaseDefended())
		{
			return whatBase.troops-this.getPower()/whatBase.getPower();
		}
		return 0;
	}
	
	void attackBase(Base whatBase)
	{
		if (this.isBaseDefended())
		{
			if (whatBase.isBaseAlive())
			{
				if (this.getPriority()<whatBase.getPriority())
				{
					int damage=this.processCasualities(whatBase);
					System.out.print("Attackers have priority");
					whatBase.removeTroops(damage);
					if (whatBase.isBaseDefended())
					{
						System.out.println(" and inflict "+damage+" casualities.");
						damage = whatBase.processCasualities(this);
						this.removeTroops(damage);
						System.out.println("Defenders counter attack inflincting "+damage+" casualities.");
					}
					else
					{
						System.out.println(" and wipe out the defenders !");
					}
				}
				else
				{
					if (this.getPriority()>whatBase.getPriority())
					{
						int damage=this.processCasualities(this);
						System.out.print("Defenders have priority");
						this.removeTroops(damage);
						if (this.isBaseDefended())
						{
							System.out.println(" and inflict "+damage+" casualities.");
							damage=this.processCasualities(whatBase);
							whatBase.removeTroops(damage);
							System.out.println("Attackers can finaly attack inflincting "+damage+" casualities.");
						}
						System.out.println(" and wipe out the attackers before they could act !");
					}
					else
					{
						int damage=this.processCasualities(whatBase);
						int damage2=whatBase.processCasualities(this);
						System.out.println("Both sides attacks at the same time, attackers inflict "+damage+" casualities but get themselves "+damage2+"casualities.");
						this.removeTroops(damage2);
						whatBase.removeTroops(damage);
					}
				}
				if (whatBase.isBaseDefended())
				{
					System.out.println("The assault has ended.");
				}
				else
				{
					whatBase.hurt();
					System.out.println("Attackers pierced the defences, inflicting one damage to the defenders !");
					if (whatBase.getLives()==0)
					{
						System.out.println(whatBase.getName()+" got too much damage and has been destroyed !");
						whatBase.destroyBase();
						for (int i=0;i<whatBase.access.size();++i)
						{
							if (!this.getAccesses().contains(whatBase.getAccesses().get(i)))
							{
								this.addBase(whatBase.getAccesses().get(i));
								if (!whatBase.getAccesses().get(i).getAccesses().contains(this))
								{
									whatBase.getAccesses().get(i).addBase(this);
								}
							}
							if (!whatBase.getAccesses().get(i).getAccesses().contains(this))
							{
								whatBase.getAccesses().get(i).getAccesses().add(this);
								whatBase.getAccesses().get(i).getAccesses().remove(whatBase);
							}
						}
						this.getAccesses().remove(whatBase);
					}
				}
				whatBase.setAttackedStatus(true);
			}
			else
			{
				System.out.println("Attack failed because the attacked base was empty...");
			}
		}
		else
		{
			System.out.println("Attack failed because there were no any troops...");
		}
	}
	
	Base getRandomAccess()
	{
		if (this.getAccesses()==null)
		{
			return null;
		}
		if (this.getAccesses().size()==0)
		{
			return null;
		}
		if (this.getAccesses().size()==1)
		{
			return this.getAccesses().get(0);
		}
		return this.getAccesses().get((int)(Math.random()*this.getAccesses().size()));
	}
	
	public String toString()
	{
		if (this.isAlive)
		{
			return "=====Base of "+this.getPlayer()+" ("+this.getPlayer().getRace()+")=====\n"
				+ "===[Population] " + this.getPop()+"\n"
				+ "===[Army]------ " + this.getTroops()+"\n"
				+ "===[Lives]----- " + this.getLives()+"\n";
		}
		return "=====Base of "+this.getPlayer()+" ("+this.getPlayer().getRace()+")=====\n[DEAD]\n";
		
	}
}
