package sturcruft_package;
import java.util.Scanner;

public class Game {
	private int playerNumber;
	private Player [] players;
	private Scanner reader = new Scanner(System.in);
	
	int getPlayerCount()
	{
		return this.playerNumber;
	}
	
	void setPlayerCount(int newAmount)
	{
		this.playerNumber=newAmount;
	}
	
	Player createPlayer()
	{
		System.out.print("Set your player name : ");
		String newName = reader.next();
		while(newName.length() < 4 || newName.length() > 30)
		{
			System.out.print("Your name must be between 4 and 30 characters : ");
			newName = reader.next();
		}
		System.out.print("Set your base name : ");
		String baseName = reader.next();
		while(baseName.length() < 4 || baseName.length() > 30)
		{
			System.out.print("Your Base name must be between 4 and 30 characters : ");
			baseName = reader.next();
		}
		System.out.print("Set your Race (Random 0, Terran 1, Zerg 2, Protoss 3) : ");
		int whatRacePicked = reader.nextInt();
		while(whatRacePicked < 0 || whatRacePicked > 3)
		{
			System.out.print("Incorrect input, try again : ");
			whatRacePicked = reader.nextInt();
		}
		Race newRace = null;
		if (whatRacePicked == 1)
		{
			newRace = Race.TERRAN;
		}
		if (whatRacePicked == 2)
		{
			newRace = Race.ZERG;
		}
		if (whatRacePicked == 3)
		{
			newRace = Race.PROTOSS;
		}
		if (whatRacePicked == 0)
		{
			newRace = Race.getRandomRace();
		}
		return new Player(newName, baseName, newRace);
	}
	
	Player createIA()
	{
		return createIA(Race.getRandomRace());
	}
	
	Player createIA(Race whatRace)
	{
		Player newIA = new Player("IA","IA BASE", whatRace);
		newIA.setToIA();
		return newIA;
	}
	Game()
	{
		this.setPlayerCount(4);
		this.players = new Player[this.getPlayerCount()];
		this.players[0] = this.createPlayer();
		this.players[1] = this.createIA();
		this.players[1].setDelay(3);
		this.players[2] = this.createIA();
		this.players[2].setDelay(3);
		this.players[3] = this.createIA();
		this.players[3].setDelay(3);
		this.players[0].getBase().addBase(this.players[2].getBase());
		this.players[1].getBase().addBase(this.players[3].getBase());
		this.players[2].getBase().addBase(this.players[3].getBase());
	}
	
	void doPlayerTurn(Player whatPlayer)
	{
		int order;
		int newPop = whatPlayer.getBase().getCurrentgrow();
		int newArmy=0;
		boolean endOfTurn=false;
		boolean attackDone=false;
		while (!endOfTurn)
		{
			if (newPop < 0)
			{
				endOfTurn=true;
			}
			System.out.print("It's the turn of "+whatPlayer+".\nYou have "+whatPlayer.getBase().getPop()+" of population, "+whatPlayer.getBase().getTroops()+" soldiers (with "+newArmy+" being trained) "
					+ "and " +newPop+" new units.\nWhat do you want to do ? (0 : end the turn, 1 : train troops");
			if (!attackDone || whatPlayer.getBase().getTroops()>0)
			{
				System.out.print(", 2 : attack");
			}
			System.out.println(")");
			order=reader.nextInt();
			if (order==0)
			{
				System.out.println("Confirm ending the turn ? (1 : yes/ 2 : no)");
				order=reader.nextInt();
				if (order==1)
				{
					if (newPop > 0)
					{
						whatPlayer.getBase().addPop(newPop);
						System.out.println("Your population grew by "+newPop+" troops.");
						newPop=0;
					}
					endOfTurn=true;
				}
				continue;
			}
			if (order==1)
			{
				while(newPop>0)
				{
					System.out.println("You have "+newPop+"\nHow many troops you want to train ?");
					order=reader.nextInt();
					if (order<0 || order>newPop)
					{
						System.out.println("Wrong input, try again");
					}
					else
					{
						int newTroops=order;
						System.out.println("Are you sure to train "+newTroops+" units ? (1 : yes/ 2 : no)");
						order=reader.nextInt();
						if (order==1)
						{
							newPop-=newTroops;
							if (whatPlayer.getRace() == Race.ZERG)
							{
								newTroops*=2;
							}
							newArmy+=newTroops;
							System.out.println("You trained "+newTroops+" troops.");
						}
						System.out.println("Continue to train units ? (1 : yes/ 2 : no)");
						order=reader.nextInt();
						if (order!=1)
						{
							break;
						}
					}
					System.out.println("Want to end the turn ? (1 : yes/ 2 : no)");
					order=reader.nextInt();
					if (order==1)
					{
						System.out.println(newPop+" population has been added.");
						whatPlayer.getBase().addPop(newPop);
						newPop=0;
					}
				}
			continue;
			}
			if (order==2)
			{
				if (attackDone)
				{
					System.out.println("Error : you already attacked");
				}
				else
				{
					System.out.println("You decided to attack someone");
					if (whatPlayer.getBase().getAccesses().size()==1)
					{
						System.out.println("You have only one target : "+whatPlayer.getBase().getAccesses().get(0).getName()+", do you want to attack (1 : yes/ 2 : no)?");
						order=reader.nextInt();
						if (order==1)
						{
							whatPlayer.getBase().attackBase(whatPlayer.getBase().getAccesses().get(0));
							attackDone=true;
						}
						else
						{
							System.out.println("You choosed to not attack.");
						}
					}
					else
					{
						System.out.println("Here's a list of targets : ");
						for (int i=0;i<whatPlayer.getBase().getAccesses().size();++i)
						{
							System.out.println("["+i+"] "+whatPlayer.getBase().getAccesses().get(i).getName());
						}
						System.out.println("Select your target by using the index (or cancel by using -1)");
						order=reader.nextInt();
						boolean finished=false;
						while (!finished)
						{
							if (order<0)
							{
								System.out.println("Are you sure to cancel ? (1 : yes/ 2 : no)");
								order=reader.nextInt();
								if (order==1)
								{
									finished=true;
								}
							}
							if (order<whatPlayer.getBase().getAccesses().size())
							{
								System.out.println("Are you sure to attack "+whatPlayer.getBase().getAccesses().get(order).getName()+" (1 : yes/ 2 : no) ? ");
								order=reader.nextInt();
								if (order==1)
								{
									finished=true;
									whatPlayer.getBase().attackBase(whatPlayer.getBase().getAccesses().get(order));
									attackDone=true;
								}
							}
						}
					}
					continue;
				}
				if (order>2 || order<0)
				{
					System.out.println("Error : try again.");
					continue;
				}
			}
		}
		whatPlayer.getBase().AddTroops(newArmy);
		System.out.println("End of the turn for"+whatPlayer);
		
	}
	
	void doIATurn(Player whatPlayer)
	{
		int newPop = whatPlayer.getBase().getCurrentgrow();
		if (/*whatPlayer.getBase().isAttacked() ||*/ whatPlayer.getBase().hasBeenAttacked())
		{
			whatPlayer.setDelay(0);
			whatPlayer.getBase().AddTroops(newPop);
			if (whatPlayer.getBase().getAccesses().size()==1)
			{
				whatPlayer.getBase().attackBase(whatPlayer.getBase().getAccesses().get(0));
			}
			else
			{
				whatPlayer.getBase().attackBase(whatPlayer.getBase().getRandomAccess());
			}
		}
		else
		{
			if (whatPlayer.getDelay()<=0)
			{
				whatPlayer.setDelay(3);
	
				if (whatPlayer.getBase().getAccesses().size()==1)
				{
					whatPlayer.getBase().attackBase(whatPlayer.getBase().getAccesses().get(0));
				}
				else
				{
					whatPlayer.getBase().attackBase(whatPlayer.getBase().getRandomAccess());
				}
			}
			else
			{
				whatPlayer.getBase().AddTroops(newPop/2);
				whatPlayer.getBase().addPop(newPop-newPop/2);
			}
		}
	}
	
	boolean isGameEmpty()
	{
		if (this.playerNumber==0)
		{
			return true;
		}
		for (byte i=0; i<this.playerNumber;++i)
		{
			if (this.players[i] != null)
			{
				return false;
			}
		}
		return true;
	}
	
	boolean isGameEnded()
	{
		boolean onePlayer=false;
		if (this.isGameEmpty()) {

			System.out.println("Game is Empty");
			return false;
		}
		for (byte i=0;i<this.playerNumber;++i)
		{
			if (!players[i].isPlayerDead())
			{
				if (!onePlayer)
				{
					System.out.println("There is at least a player");
					onePlayer=true;
				}
				else
				{
					System.out.println("There's a second player : Game is not ended");
					return false;
				}
			}
		}
		if (onePlayer)
		{
			System.out.println("there's only one player");
			return true;
		}

		System.out.println("there's no any players");
		return false;
	}
	
	boolean isGameCorrect()
	{
		if (this.isGameEmpty())
		{
			return false;
		}
		return !this.isGameEnded();
		
	}
	void startGame()
	{
		if (!this.isGameCorrect())
		{
			System.out.println("Error : some players are not set");
			for (int i=0;i<this.playerNumber;++i)
			{
				if (this.players[i]==null)
				{
					System.out.println("Player "+i+" is null");
				}
				else
				{
					if (this.players[i].isPlayerDead())
					{
						System.out.println("Player "+i+" is dead lol");
					}
				}

			}
		}
		else
		{
			while(!this.isGameEnded())
			{
				for (int i=0;i<this.playerNumber;++i)
				{
					System.out.println(this.toString());
					if (this.players[i].isAPlayer())
					{
						this.doPlayerTurn(players[i]);
					}
					if (this.players[i].isAnIA())
					{
						this.players[i].downDelay();
						this.doIATurn(players[i]);
					}
				}
				for (int i=0;i<this.playerNumber;++i)
				{
					if (this.players[i].getBase().isBaseAlive())
					{
						if (this.players[i].getBase().isAttacked())
						{
							this.players[i].getBase().setWasAttackedStatus(true);
							this.players[i].getBase().setAttackedStatus(false);
						}
						else
						{
							if (this.players[i].getBase().hasBeenAttacked())
							{
								this.players[i].getBase().setWasAttackedStatus(false);
							}
							else
							{
								this.players[i].getBase().regenerate();
							}
						}
					}
				}
			}
			System.out.print("The winner is : ");
			for (int i=0;i<this.playerNumber;++i)
			{
				if (this.players[i].getBase().isBaseAlive())
				{
					System.out.print(this.players[i].getName());
					break;
				}
			}
			System.out.println(" !");
		}
	}
	
	public String toString()
	{
		String s="";
		if (this.playerNumber==0)
		{
			return "Game is not initialized";
		}
		for (int i=0;i<this.playerNumber;++i)
		{
			s+=this.players[i].getBase();
		}
		return s;
	}
}
