package sturcruft_package;

public class Player {
	private String name;
	private boolean isDead;
	private Base base;
	private Race race;
	private boolean isIA;
	private int delay;
	
	void setDelay(int newDelay)
	{
		if (this.isAnIA())
		{
			this.delay=newDelay;
		}
	}
	
	void downDelay()
	{
		this.setDelay(this.getDelay()-1);
	}
	
	int getDelay()
	{
		return this.delay;
	}
	String getName()
	{
		return this.name;
	}
	void setName(String newName)
	{
		this.name=newName;
	}
	
	boolean isPlayerDead()
	{
		return this.isDead;
	}
	void kill()
	{
		this.isDead=true;
	}
	void resurect()
	{
		this.isDead=false;
	}
	
	Base getBase()
	{
		return this.base;
	}
	void setBase(Base newBase)
	{
		if (this.getBase() == null)
		{
			this.base = newBase;
		}
	}
	
	void setToPlayer()
	{
		this.isIA=false;
	}
	void setToIA()
	{
		this.isIA=true;
	}
	boolean isAnIA()
	{
		return this.isIA;
	}
	boolean isAPlayer()
	{
		return !this.isIA;
	}
	
	Race getRace()
	{
		return this.race;
	}
	
	void setRace(Race newRace)
	{
		this.race=newRace;
	}
	
	Player(String newName, String baseName, Race whatRace)
	{
		this.setToPlayer();
		this.setName(newName);
		this.setRace(whatRace);
		this.setBase(new Base(whatRace,baseName));
		this.getBase().setPlayer(this);
		assert(this.getBase()==null);
		this.resurect();
	}
	
	Player()
	{
		this("NoName","NoBaseName",null);
	}
	
	public String toString()
	{
		return this.name;
	}
}
